#include "joy2cvgstack/joyconvertermidlevelautopilotcmd.h"

JoyConverterDrvPelicanCmd::JoyConverterDrvPelicanCmd() : DroneModule(droneModule::active)
{
    init();
    return;
}

JoyConverterDrvPelicanCmd::~JoyConverterDrvPelicanCmd()
{

    return;
}

void JoyConverterDrvPelicanCmd::init()
{
    return;
}

void JoyConverterDrvPelicanCmd::close()
{
    return;
}

void JoyConverterDrvPelicanCmd::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    OutputPubl_PR = n.advertise<droneMsgsROS::dronePitchRollCmd>(DRONE_DRIVER_COMMAND_DRONE_COMMAND_PITCH_ROLL, 1, true);
    OutputPubl_dY = n.advertise<droneMsgsROS::droneDYawCmd>(     DRONE_DRIVER_COMMAND_DRONE_COMMAND_DYAW,       1, true);
    OutputPubl_dA = n.advertise<droneMsgsROS::droneDAltitudeCmd>(DRONE_DRIVER_COMMAND_DRONE_COMMAND_DALTITUDE,  1, true);

    const std::string DRONE_DRIVER_COMMAND_DRONE_COMMAND_PITCH_ROLL  = "command/pitch_roll";
    const std::string DRONE_DRIVER_COMMAND_DRONE_COMMAND_DALTITUDE   = "command/dAltitude";
    const std::string DRONE_DRIVER_COMMAND_DRONE_COMMAND_DYAW        = "command/dYaw";

    //Subscriber
    InputSubs=n.subscribe("joy", 1, &JoyConverterDrvPelicanCmd::inputCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool JoyConverterDrvPelicanCmd::resetValues()
{
    return true;
}

//Start
bool JoyConverterDrvPelicanCmd::startVal()
{
    return true;
}

//Stop
bool JoyConverterDrvPelicanCmd::stopVal()
{
    return true;
}

//Run
bool JoyConverterDrvPelicanCmd::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

//#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void JoyConverterDrvPelicanCmd::inputCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

//  # Received input from joypad "Thrustmaster, Dual Analog 3", note that this
//  #   joypad is very low quality, don't fly the Pelican with it!!!!
//  #     msg->axes[0] // stick: left -horizontal, left:+1 right:-1 //   dyaw
//  #     msg->axes[1] // stick: left -vertical  ,   up:+1  down:-1 // thrust
//  #     msg->axes[2] // stick: right-horizontal, left:+1 right:-1 //   roll
//  #     msg->axes[3] // stick: right-vertical  ,   up:+1  down:-1 //  pitch
//  std::cout << " pitch:" << msg->axes[3] << std::endl;
//  std::cout << "  roll:" << msg->axes[2] << std::endl;
//  std::cout << "  dyaw:" << msg->axes[0] << std::endl;
//  std::cout << "thrust:" << msg->axes[1] << std::endl;

//  # Desired joypad-vs-pelican behaviour
//  #   signs in the code are assigned to obtain the desired behaviour.
//  #   The yaw sign test was not done during flight, so it might be wrong.
//  #     stick: left -horizontal, left:      dyaw-leftwards   right:        dyaw-rightwards
//  #     stick: left -vertical  ,   up:     thrust-upwards     down:     thrust-downwards
//  #     stick: right-horizontal, left: horizontal-left       right: horizontal-left
//  #     stick: right-vertical  ,   up: horizontal-frontwards  down: horizontal-backwards

//  # Mavwork sign reference system, sign modifiers are (+/-1.0), all seem to be (+1.0):
//  # [pitch][+] move backwards
//  # [pitch][-] move forward
//  # [roll][+] move rightwards
//  # [roll][-] move leftwards
//  # [dyaw][+][speed command] rotate clockwise (as seen from above), or N > E > S > W > N > ...
//  # [dyaw][-][speed command] rotate counter-clockwise (as seen from above), or N > W > S > E > N > ...
//  # [dz][+][speed command] increase altitude, move upwards
//  # [dz][-][speed command] decrease altitude, move downwards
    droneMsgsROS::dronePitchRollCmd msg_PR;
    droneMsgsROS::droneDYawCmd      msg_dY;
    droneMsgsROS::droneDAltitudeCmd msg_dA;

    msg_PR.pitchCmd     = (float) (-1.0)*( msg->axes[3] );
    msg_PR.rollCmd      = (float) (-1.0)*( msg->axes[2] );
    msg_dY.dYawCmd      = (float) (-1.0)*( msg->axes[0] );
    msg_dA.dAltitudeCmd = (float) (+1.0)*( msg->axes[1] );

    msg_PR.header.stamp = ros::Time::now();
    msg_dY.header.stamp = ros::Time::now();
    msg_dA.header.stamp = ros::Time::now();

    //Publish
    OutputPubl_PR.publish(msg_PR);
    OutputPubl_dY.publish(msg_dY);
    OutputPubl_dA.publish(msg_dA);
    return;
}
